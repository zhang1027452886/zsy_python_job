# ！usr/bin/env python
# _*_ coding: utf-8 _*_


class Calculator:
    def add(self, a, b):
        if not (isinstance(a, int) or isinstance(a, float)) or not (isinstance(b, int) or isinstance(b, float)):
            raise TypeError
        else:
            return a + b

    def sub(self, a, b):
        return a - b

    def mul(self, a, b):
        if not (isinstance(a, int) or isinstance(a, float)) or not (isinstance(b, int) or isinstance(b, float)):
            raise TypeError
        else:
            return a * b

    def div(self, a, b):
        return a / b
