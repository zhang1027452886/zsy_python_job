# ！usr/bin/env python
# _*_ coding: utf-8 _*_
import logging

import pytest

from calculator_job import calculator


@pytest.fixture()
def fixture_fun(begin,end):
    calculi = calculator.Calculator()
    logging.info(begin)
    yield calculi
    logging.info(end)
