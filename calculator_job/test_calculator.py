# ！usr/bin/env python
# _*_ coding: utf-8 _*_
import logging

import allure
import pytest
from calculator_job.datas import open_yaml


@allure.feature("计算器测试")
class TestCalculator:

    # 加法测试
    @allure.story("整数相加")
    @pytest.mark.parametrize('begin, a, b, resu,end', open_yaml()["add_int"], ids=open_yaml()["add_int_ids"])
    def test_add_int(self, fixture_fun, begin, a, b, resu,end):
        assert resu == fixture_fun.add(a, b)

    @allure.story("浮点数相加")
    @pytest.mark.parametrize('begin, a, b, resu,end', open_yaml()["add_float"], ids=open_yaml()["add_float_ids"])
    def test_add_float(self,fixture_fun, begin, a, b, resu,end):
        assert round(resu,3) == round(fixture_fun.add(a, b),3)

    @allure.story("字符串相加")
    @pytest.mark.parametrize('begin, a, b, resu,end', open_yaml()["add_str"], ids=open_yaml()["add_str_ids"])
    def test_add_str(self, fixture_fun, begin, a, b, resu, end):
        with pytest.raises(TypeError):
            fixture_fun.add(a, b)

    # 减法测试
    @allure.story("整数相减")
    @pytest.mark.parametrize('begin, a, b, resu,end', open_yaml()["sub_int"], ids=open_yaml()["sub_int_ids"])
    def test_sub_int(self, fixture_fun, begin, a, b, resu, end):
        assert resu == fixture_fun.sub(a, b)

    @allure.story("浮点数相减")
    @pytest.mark.parametrize('begin, a, b, resu,end', open_yaml()["sub_float"], ids=open_yaml()["sub_float_ids"])
    def test_sub_float(self, fixture_fun, begin, a, b, resu, end):
        assert resu == round(fixture_fun.sub(a, b),4)

    @allure.story("字符串相减")
    @pytest.mark.parametrize('begin, a, b, resu,end', open_yaml()["sub_str"], ids=open_yaml()["sub_str_ids"])
    def test_sub_str(self, fixture_fun, begin, a, b, resu, end):
        with pytest.raises(TypeError):
            fixture_fun.sub(a, b)
            
            
    # 乘法测试
    @allure.story("整数相乘")
    @pytest.mark.parametrize('begin, a, b, resu,end', open_yaml()["mul_int"], ids=open_yaml()["mul_int_ids"])
    def test_mul_int(self, fixture_fun, begin, a, b, resu, end):
        assert resu == fixture_fun.mul(a, b)

    @allure.story("浮点数相乘")
    @pytest.mark.parametrize('begin, a, b, resu,end', open_yaml()["mul_float"], ids=open_yaml()["mul_float_ids"])
    def test_mul_float(self, fixture_fun, begin, a, b, resu, end):
        assert resu == round(fixture_fun.mul(a, b),5)

    @allure.story("字符串相乘")
    @pytest.mark.parametrize('begin, a, b, resu,end', open_yaml()["mul_str"], ids=open_yaml()["mul_str_ids"])
    def test_mul_str(self, fixture_fun, begin, a, b, resu, end):
        with pytest.raises(TypeError):
            fixture_fun.mul(a, b)

    # 除法测试
    @allure.story("整数相除")
    @pytest.mark.parametrize('begin, a, b, resu,end', open_yaml()["div_int"], ids=open_yaml()["div_int_ids"])
    def test_div_int(self, fixture_fun, begin, a, b, resu, end):
        assert resu == round(fixture_fun.div(a, b),2)

    @allure.story("浮点数相除")
    @pytest.mark.parametrize('begin, a, b, resu,end', open_yaml()["div_float"], ids=open_yaml()["div_float_ids"])
    def test_div_float(self, fixture_fun, begin, a, b, resu, end):
        assert resu == round(fixture_fun.div(a, b), 2)

    @allure.story("字符串相除")
    @pytest.mark.parametrize('begin, a, b, resu,end', open_yaml()["div_str"], ids=open_yaml()["div_str_ids"])
    def test_div_str(self, fixture_fun, begin, a, b, resu, end):
        with pytest.raises(TypeError):
            fixture_fun.div(a, b)

    @allure.story("除数为0")
    @pytest.mark.parametrize('begin, a, b, resu,end', open_yaml()["div_zero"], ids=open_yaml()["div_zero_ids"])
    def test_div_zero(self, fixture_fun, begin, a, b, resu, end):
        with pytest.raises(ZeroDivisionError):
            fixture_fun.div(a, b)

