# ！usr/bin/env python
# _*_ coding: utf-8 _*_
import money as e

def send_salary(salary):
    #函数返回工资参数salary的值
    #已知正常工资1000元，判断工资发放情况

    if salary < 0:
        print("不能扣我工资")

    elif salary == 0:
        print("快发工资")

    elif salary < 1000:
        print("工资发少了")

    elif salary == 1000:
        print("工资无差异")

    else:
        print(f"正常工资1000元，谢谢老板奖金{salary-1000}元，下次继续")

    return salary



