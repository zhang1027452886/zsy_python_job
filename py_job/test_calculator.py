# ！usr/bin/env python
# _*_ coding: utf-8 _*_
import logging

import pytest
from py_job.calculator import Calculator

from decimal import *


class TestCalculator:
    def setup_class(self):
        logging.info('开始计算')
        self.calcu = Calculator()


    def teardown_class(self):
        logging.info('结束计算')


    # 加法测试
    @pytest.mark.myaddtest
    @pytest.mark.parametrize('a, b, cal_sum', [
        [1, 1, 2], [2.5,3.4,5.9],[-1,-5,-6],['a',2,TypeError],['ab','cd',TypeError]
    ],ids=['int+int','float+float','minus+minus','char+num','char+char'])
    def test_add(self, a, b, cal_sum):
        if not (isinstance(a,int) or isinstance(a,float)) or not (isinstance(b,int) or isinstance(b,float)):
            with pytest.raises(TypeError):
                self.calcu.add(a, b)
        else:
            assert cal_sum == self.calcu.add(a,b)


    # 减法测试
    @pytest.mark.mysubtest
    @pytest.mark.parametrize('a, b, cal_balance', [
        [1, 1, 0], [2.1,3.2,-1.1],[-1,-5,4],[-8,5,-13],['a',2,TypeError],['ab','cd',TypeError]
    ],ids=['int-int','float-float','minus-minus','minus-int','char-num','char-char'])
    def test_sub(self, a, b, cal_balance):
        if not (isinstance(a,int) or isinstance(a,float)) or not (isinstance(b,int) or isinstance(b,float)):
            with pytest.raises(TypeError):
                self.calcu.sub(a, b)
        else:
            assert cal_balance == self.calcu.sub(a,b)


    # 乘法测试
    @pytest.mark.mymultest
    @pytest.mark.parametrize('a, b, cal_product', [
        [11, 12, 132], [2.3,2,4.6],[-1,-5,5],[-8,5,-40],[512.3,0,0],[0,556,0],['a',2,TypeError],['ab','cd',TypeError]
    ],ids=['int*int','float*float','minus*minus','minus*int','float*0','0*int','char*num','char*char'])
    def test_mul(self, a, b, cal_product):
        if not (isinstance(a,int) or isinstance(a,float)) or not (isinstance(b,int) or isinstance(b,float)):
            with pytest.raises(TypeError):
                self.calcu.mul(a, b)
        else:
            assert cal_product == self.calcu.mul(a,b)


    # 除法测试
    @pytest.mark.mydivtest
    @pytest.mark.parametrize('a, b, cal_consult',[
        [9,3,3],[9,2,4.5],[3.6,6,0.6],[-50,5,-10],[-100,-20,5],[0,555,0],[1,3,0.3333333333333333],['a',2,TypeError],[5,'cb',TypeError],['ab','cd',TypeError],[1,0,'ZeroDivisionError']
    ],ids=['int/int','exactdiv','float/int','minus/int','minus/minus','0/int','circulating','char/num','num/char','char/char','/0'])
    def test_div(self, a, b, cal_consult):
        if b == 0:
            with pytest.raises(ZeroDivisionError):
                self.calcu.div(a, b)
        elif not (isinstance(a,int) or isinstance(a,float)) or not (isinstance(b,int) or isinstance(b,float)):
            with pytest.raises(TypeError):
                self.calcu.div(a, b)
        else:
            assert cal_consult == self.calcu.div(a,b)









