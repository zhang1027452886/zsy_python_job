# ！usr/bin/env python
# _*_ coding: utf-8 _*_

class Animal:
    def __init__(self, name, color, age, gender):
        self.name = name
        self.color = color
        self.age = age
        self.gender = gender

    def call(self):
        print(f"名字：{self.name}，颜色：{self.color}，年龄：{self.age}，性别：{self.gender}   会叫")

    def run(self):
        print(f"名字：{self.name}，颜色：{self.color}，年龄：{self.age}，性别：{self.gender}   会跑")


class Cat(Animal):
    def __init__(self, name, color, age, gender, hair):

        super().__init__(name, color, age, gender)
        self.hair = hair

    def catch_mouse(self):
        print(f"大家好，我是{self.name}，我全身长满{self.color}的{self.hair}，我已经{self.age}岁了，我是个{self.gender}, 我会捉老鼠")

    def call(self):
        print(f"大家好，我是{self.name}，我全身长满{self.color}的{self.hair}，我已经{self.age}岁了，我是个{self.gender}, 我会喵喵叫")

class Dog(Animal):
    def __init__(self, name, color, age, gender, hair):
        self.hair = hair
        super().__init__(name, color, age, gender)


    def watch_house(self):
        print(f"大家好，我是{self.name}，我全身长满{self.color}的{self.hair}，我已经{self.age}岁了，我是个{self.gender}, 我会看家")

    def call(self):
        print(f"大家好，我是{self.name}，我全身长满{self.color}的{self.hair}，我已经{self.age}岁了，我是个{self.gender}, 我会汪汪叫")



if __name__ == '__main__':
    cat = Cat('小懒猫', '灰色', '2', '小女生', '短毛')
    dog = Dog('金毛', '金色', '3', '小男生', '长毛')
    cat.catch_mouse()
    cat.call()
    dog.call()
    dog.watch_house()
